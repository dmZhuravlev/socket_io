var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var expressValidator = require('express-validator');

var app = express();

var server = require('http').createServer(app);
var io = require('socket.io')(server);

app.use(bodyParser.json());
app.use(expressValidator());
app.use(express.static(__dirname + '/build'));
mongoose.connect('mongodb://zhur:access@ds053788.mongolab.com:53788/storage');

require('./backend/model/user.js');
require('./backend/chat/rooms.js')(io);
require('./backend/config/routes.js')(app);

//app.listen({
//  port: 3001,
//  host: '10.10.54.28'
//});

//http://socket.io/docs/#using-with-express-3/4
server.listen(3001);

console.log("App listening on port ", 3001);
