var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = new Schema({
    name: {
      type: String,
      unique: true
    },
    password: String,
    itemList: [{
      done: Boolean,
      text: String
    }]
}, {
    collection: 'users'
});

module.exports = mongoose.model('User', userSchema);