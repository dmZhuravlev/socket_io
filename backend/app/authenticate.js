var mongoose = require('mongoose');
var user = mongoose.model('User');
var jwt = require('jsonwebtoken');

/*
 * SECRET KEY (We are going to protect /api routes with JWT)
 */
var secret = 'this is the secret secret secret 12356';

module.exports = function (req, res) {
    console.log('req.body.username', req.body.username);
    console.log('req.body.pass', req.body.password);

    user.find({
                'name': req.body.username,
                'password': req.body.password
            }, function (err, item) {

        if (err) {
            return res.send("contact addMsg error: " + err);
        }

        if(!item.length) {
            res.status(401).send('Wrong user or password');
            return;
        }
        var profile = {
            idUser: item[0]._id
        };

        // We are sending the profile inside the token
        var token = jwt.sign(profile, secret, { 
            expiresIn : 60*60*24 
        });
        res.json({ token: token });
    });
};
