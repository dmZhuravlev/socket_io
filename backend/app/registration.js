/*
* Registration new user
*/
var mongoose = require('mongoose');
var user = mongoose.model('User');
var jwt = require('jsonwebtoken');

/*
 * SECRET KEY (We are going to protect /api routes with JWT)
 */
var secret = 'this is the secret secret secret 12356';
module.exports =  function (req, res) {
    var errors = null;
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();

    errors = req.validationErrors();

    if (!errors) {
        user.find({
            'name': req.body.name
        }, function (err, item) {
            if (err) return res.send("contact addMsg error: " + err);
            console.log('test', item);
            if (!item.length) {
                var userElem = new user({
                    name: req.body.name,
                    password: req.body.password,
                    itemList : []
                });
                var setToken = function (itemEl) {
                    var profile = {
                        idUser: itemEl._id
                    };

                    // We are sending the profile inside the token
                    var token = jwt.sign(profile, secret, { 
                        expiresIn : 60*60*24 
                    });
                    res.json({ token: token });
                };

                userElem.save(function (err, data) {
                    if (err) {
                        return res.send(401, 'Error' + err);
                    } else {
                        console.log('Saved ', data);
                        user.findOne({
                            'name': data.name
                        }, function (err, item) {
                            if (err) return res.send("contact addMsg error: " + err);
                            item && setToken(item);
                        });
                    }
                });
            } else {
                return res.send(401, "This name is already exist");
            }

        });
    } else {
        console.log('errors', errors);
        return res.send(401, ' Error' + errors[0].msg);
    }
};