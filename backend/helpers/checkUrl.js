module.exports = function (data, res) {
    var cheerio = require('cheerio');
    var $;
    var description = '';
    var url = data.request.url;

    if (/image/g.test(data.headers['content-type'])) {
        return res.status(200).send({
            'img': url
        });
    }
    if (!data.text) {
        return res.status(400).send('url is not correct');
    }

    $ = cheerio.load(data.text);

    $('meta').each(function (i, element) {
        if ($(element).attr('name') === 'description') {
            description = $(element).attr('content');
        }
    });

    res.status(200).send({
        'title': $('title').text(),
        'url': url,
        'description': description
    });
};