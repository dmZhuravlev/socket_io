module.exports = function (app) {
    app.post('/api/authenticate', require('../app/authenticate'));
    app.post('/api/registration', require('../app/registration'));
};
