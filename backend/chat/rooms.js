module.exports = function (io) {
    var numUsers = 0;
    var userList = {};

    io.on('connection', function (socket) {
        var addedUser = false;
        
        // when the client emits 'new message', this listens and executes
        socket.on('new message', function (data) {

            // we tell the client to execute 'new message'
            socket.broadcast.emit('new message', {
                username: data.username,
                message: data.message
            });
        });

        // when the client emits 'add user', this listens and executes
        socket.on('add user', function (username) {
            if (addedUser) return;

            // we store the username in the socket session for this client
            socket.username = username;
            ++numUsers;
            addedUser = true;
            userList[username] = socket.id;

            socket.emit('users list', {
                list: userList
            })

            socket.broadcast.emit('users list', {
                list: userList
            });
        });

        socket.on('particular user', function (data) {
            socket.broadcast.to(data.id).emit('new message', {
                message : data.message,
                username: data.username
            });
        });

        // when the client emits 'typing', we broadcast it to others
        socket.on('typing', function () {
            socket.broadcast.emit('typing', {
                username: socket.username
            });
        });

        // when the client emits 'stop typing', we broadcast it to others
        socket.on('stop typing', function () {
            socket.broadcast.emit('stop typing', {
                username: socket.username
            });
        });

        // when the user disconnects.. perform this
        socket.on('disconnect', function () {
            if (addedUser) {
                --numUsers;

                delete userList[socket.username];
                socket.broadcast.emit('users list', {
                    list: userList
                });

                // echo globally that this client has left
                socket.broadcast.emit('user left', {
                    username: socket.username,
                    numUsers: numUsers
                });
            }
        });
    });
}