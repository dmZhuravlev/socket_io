import template from './registration.html';

export default {
    template,
    controller: class Registration {
        constructor($state, RegisterService) {
            'ngInject';
            this.RegisterService = RegisterService;
            this.$state = $state;
        }

        submitForm({ $valid }) {
            if($valid) {
                this.RegisterService.createAccount(this.username, this.password).then(() => {
                    this.$state.go('home');
                });
            }
        }
    }
};