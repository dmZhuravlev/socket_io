import RegistrationComponent from './registration.component';
import './registration.scss';

export default angular
    .module('app.components.registration', [])
    .component('registration', RegistrationComponent)
    .name;