import controller from './controllers';

export default () => ({
    controller,
    controllerAs: '$ctrl'
});
