import template from './login.html';

export default {
    template,
    controller: class Login {
        constructor($state, AuthService, $window) {
            'ngInject';
            this.AuthService = AuthService;
            this.$state = $state;
            this.$window = $window;
        }

        submitForm({ $valid }) {
            if($valid) {
                this.AuthService.login(this.username, this.password).then((response) => {
                    this.$window.sessionStorage.token = response.data.token;
                    this.$window.sessionStorage.name = this.username;
                    this.$state.go('home');
                });
            }
        }
    }
};