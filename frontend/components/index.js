import angular from 'angular';

import Login from './login';
import Registration from './registration';
import ChatRooms from './chat-rooms';
import MainWrapComponent from './components';

export default angular.module('app.components', [
    Login,
    Registration,
    ChatRooms
]).component('mainWrap', MainWrapComponent).name;
