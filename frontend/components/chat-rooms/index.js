import ChatRooms from './chat-rooms.component';
import './chat-rooms.scss';

export default angular
    .module('app.components.chat-rooms', [])
    .component('chatRooms', ChatRooms)
    .name;