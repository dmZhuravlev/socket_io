import template from './chat-rooms.html';
import ChatRooms from './chat-rooms.controller';

export default {
    template,
    controller: ChatRooms,
};