import io from 'socket.io-client';

class ChatRooms {
    constructor($scope, $window) {
        'ngInject';
        this.$scope = $scope;
        this.messages = [];
        this.userList = [];
        this.$window = $window;
        this.init();
    }

    init() {
        if (!this.$window.sessionStorage.token) {
            return;
        }

        this.userName = this.$window.sessionStorage.name;
        this.socket = io.connect('http://127.0.0.1:3001');

        this.socket.on('new message', (data) => {
            this.addChatMessage(data);
        });

        this.socket.on('users list', (data) => {
            this.renderUserList(data);
        });
        this.socket.emit('add user', this.userName);
    }

    sendMessage() {
        const data = {
            username: this.userName,
            message: this.$scope.message
        }

        this.socket.emit('new message', data);
        this.addChatMessage(data, true);
        this.$scope.message = '';
    }

    testClick(user) {
        this.socket.emit('particular user', {
            id: user.id,
            message: 'private test message from' + this.userName,
            username: this.userName
        });
    }

    renderUserList(data) {
        this.userList = [];
        for (let prop in data.list) {
            if (prop !== this.userName) {
                this.userList.push({
                    name: prop,
                    id: data.list[prop]
                })
            }
        }
        
        this.$scope.$digest();
    }

    addChatMessage(data, isMyMessage) {
        this.messages.push({
            username: data.username,
            message: data.message
        });

        (!isMyMessage) && this.$scope.$digest();
    }
}

export default ChatRooms;