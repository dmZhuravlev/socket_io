import angular from 'angular';

import 'bootstrap/dist/css/bootstrap.css';
import './index.scss';
import 'angular-ui-bootstrap';

import uiRouter from 'angular-ui-router';

import Components from './components';
import ServicesModule from './services';


angular.module('app', [ServicesModule, Components, 'ui.bootstrap', uiRouter])
    .factory('authInterceptor', function($rootScope, $q, $window) {
        return {
            request: function(config) {
                config.headers = config.headers || {};
                if ($window.sessionStorage.token) {
                    config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
                }
                return config;
            },
            responseError: function(rejection) {
                if (rejection.status === 401) {
                // handle the case where the user is not authenticated
                }
                return $q.reject(rejection);
            }
        };
    })
    .run(function($rootScope, $location, $window) {

        $rootScope.$on('$stateChangeStart', function(event, nextRoute, currentRoute) {
            if (nextRoute != null &&
                nextRoute.access != null &&
                nextRoute.access.requiredAuthentication &&
                !$window.sessionStorage.token) {

                $location.path('/login');
            }
        });
    })
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

        $httpProvider.interceptors.push('authInterceptor');

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('login', {
                url: '/login',
                template: '<login></login>'
            })
            .state('registration', {
                url: '/registration',
                template: '<registration></registration>'
            })
            .state('home', {
                url: '/',
                template: '<chat-rooms></chat-rooms>',
                access: { requiredAuthentication: true }
            });
    });
