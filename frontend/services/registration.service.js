export default ($http) => {
    'ngInject';

    return {
        createAccount: (name, password) => {
            return $http.post('/api/registration', { name, password })
            .then(res => {

                return res;
            });
        }
    }
}
