export default ($http) => {
    'ngInject';

    return {
        login: (username, password) => {
            return $http.post('/api/authenticate', { username, password })
            .then(res => {

                return res;
            });
        },
        logout: () => {
            
        },
        getCurrentUser() {
            return false;
        },
        isAuthorized() {
            return Boolean(this.getCurrentUser());
        }
    }
}
