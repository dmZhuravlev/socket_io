import AuthService from './auth.service';
import RegisterService from './registration.service';

export default angular
    .module('app.services', [])
    .factory('AuthService', AuthService)
    .factory('RegisterService', RegisterService)
    .name